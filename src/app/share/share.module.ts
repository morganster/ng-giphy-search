import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwearFilterPipe } from './pipes/swear-filter.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [SwearFilterPipe],
  imports: [CommonModule, NgxPaginationModule, MatInputModule, MatButtonModule, MatCardModule, MatGridListModule],
  exports: [SwearFilterPipe, NgxPaginationModule, MatInputModule, MatButtonModule, MatCardModule, MatGridListModule]
})
export class ShareModule {}
