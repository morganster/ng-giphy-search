import { Pipe, PipeTransform } from '@angular/core';
import * as badWords from 'bad-words';
@Pipe({
  name: 'swearFilter'
})
export class SwearFilterPipe implements PipeTransform {
  transform(value: string, ...args: any[]): any {
    const filter = new badWords({ placeHolder: '\u2665' });
    return filter.clean(value);
  }
}
