import { SwearFilterPipe } from './swear-filter.pipe';

describe('SwearFilterPipe', () => {
  it('filter a swear word', () => {
    const pipe = new SwearFilterPipe();
    expect(pipe.transform('fuck')).toBe('♥♥♥♥');
  });

  it('filter multiple swear words', () => {
    const pipe = new SwearFilterPipe();
    expect(pipe.transform('fuck this shit, :D')).toBe('♥♥♥♥ this ♥♥♥♥, :D');
  });
});
