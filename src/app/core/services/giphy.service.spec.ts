import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { GiphyService, GiphyPagination, GiphyResponse } from './giphy.service';
import { Giphy } from 'src/app/modules/home/models/giphy';
import { HttpClient, HttpEvent } from '@angular/common/http';

describe('GiphyService', () => {
  let service: GiphyService;
  let httpMock: HttpTestingController;

  const pagination: GiphyPagination = {
    count: 1,
    offset: 1,
    total_count: 1
  };
  const expectedGifs: Giphy[] = [
    {
      bitly_url: '',
      bitly_gif_url: 'A',
      content_url: '',
      embed_url: '',
      id: '',
      images: { original: { url: '' } },
      url: '',
      import_datetime: '',
      is_sticker: 1,
      rating: '',
      slug: '',
      source: '',
      source_post_url: '',
      source_tld: '',
      title: '',
      trending_datetime: '',
      type: '',
      username: ''
    }
  ];

  const expectedData = {
    data: expectedGifs,
    pagination
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GiphyService]
    });

    service = TestBed.get(GiphyService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('returned Observable should match the right data', () => {
    service.search('some', 1).subscribe(response => {
      expect(response.pagination.count).toEqual(1);
    });

    const simpleReq = httpMock.expectOne(
      req => req.method === 'GET' && req.url === 'http://api.giphy.com/v1/gifs/search'
    );

    expect(simpleReq.request.method).toEqual('GET');

    simpleReq.flush(expectedData);
  });
});
