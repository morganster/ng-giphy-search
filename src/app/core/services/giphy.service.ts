import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Giphy } from 'src/app/modules/home/models/giphy';
export interface GiphyResponse {
  data: Giphy[];
  pagination: GiphyPagination;
}
export interface GiphyPagination {
  total_count: number;
  count: number;
  offset: number;
}
@Injectable({
  providedIn: 'root'
})
export class GiphyService {
  apiUrl = '';
  apiKey = '';
  itemByPage = 12;
  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
    this.apiKey = environment.apiKey;
  }

  search(searchTerm: string, page: number) {
    const offset = (page - 1) * this.itemByPage;
    return this.http.get<GiphyResponse>(this.apiUrl, {
      params: {
        api_key: this.apiKey,
        limit: this.itemByPage.toString(),
        q: searchTerm,
        offset: offset.toString()
      }
    });
  }
}
