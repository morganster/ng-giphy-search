import { Component, OnInit } from '@angular/core';
import { GiphyService, GiphyPagination } from 'src/app/core/services/giphy.service';
import { take } from 'rxjs/operators';
import { Giphy } from '../../models/giphy';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  public p = 1;
  public imageData: Giphy[] = [];
  private paginationData: GiphyPagination;
  private searchTerm = '';

  constructor(private giphyService: GiphyService) {}

  ngOnInit() {}

  searchAction(searchTerm: string, page?: number) {
    this.searchTerm = searchTerm;
    this.giphyService
      .search(searchTerm, page || 1)
      .pipe(take(1))
      .subscribe(data => {
        if (data) {
          this.imageData = data.data;
          this.paginationData = data.pagination;
          this.p = page || 1;
        }
      });
  }

  pageChange(page: number) {
    this.searchAction(this.searchTerm, page);
  }
}
