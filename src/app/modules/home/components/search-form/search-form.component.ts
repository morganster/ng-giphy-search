import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SwearFilterPipe } from 'src/app/share/pipes/swear-filter.pipe';
@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.sass'],
  providers: [SwearFilterPipe]
})
export class SearchFormComponent implements OnInit {
  @Output() doSearch = new EventEmitter<string>();
  myForm: FormGroup;

  constructor(private builder: FormBuilder, private swearFilter: SwearFilterPipe) {
    this.myForm = builder.group({
      inputSearch: ['', Validators.required]
    });
    this.myForm.valueChanges.subscribe(val => {
      if (typeof val.inputSearch === 'string') {
        const maskedVal = this.swearFilter.transform(val.inputSearch);
        if (val.inputSearch !== maskedVal) {
          this.myForm.patchValue({ inputSearch: maskedVal });
        }
      }
    });
  }

  ngOnInit() {}

  search() {
    if (this.myForm.valid) {
      this.doSearch.emit(this.myForm.get('inputSearch').value);
    }
  }
}
