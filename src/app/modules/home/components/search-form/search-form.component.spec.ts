import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchFormComponent } from './search-form.component';
import { SwearFilterPipe } from 'src/app/share/pipes/swear-filter.pipe';
import { MatInputModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('SearchFormComponent', () => {
  let component: SearchFormComponent;
  let fixture: ComponentFixture<SearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchFormComponent, SwearFilterPipe],
      imports: [MatInputModule, BrowserAnimationsModule, ReactiveFormsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be false if is empty', () => {
    expect(component.myForm.valid).toBeFalsy();
  });

  it('it should be valid with the input filled', () => {
    const input = component.myForm.controls.inputSearch;
    input.setValue('test');
    expect(input.valid).toBeTruthy();
  });

  it('it should be have filled with hearts in case of swear words', () => {
    const input = component.myForm.controls.inputSearch;
    input.setValue('fuck');
    expect(input.value).toBe('♥♥♥♥');
  });

  it('it should  call search', () => {
    const spy = spyOn(component, 'search').and.callThrough();
    const input = component.myForm.controls.inputSearch;
    input.setValue('some');
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    expect(component).toBeDefined();
    expect(spy);
    expect(component.search).toHaveBeenCalled();
  });

  it('it should no call search if is invalid', () => {
    spyOn(component.doSearch, 'emit');

    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();

    expect(component.doSearch.emit).toHaveBeenCalledTimes(0);
  });
});
