import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-giphy-card',
  templateUrl: './giphy-card.component.html',
  styleUrls: ['./giphy-card.component.sass']
})
export class GiphyCardComponent implements OnInit {
  @Input() gifUrl: string;
  constructor() {}

  ngOnInit() {}
}
