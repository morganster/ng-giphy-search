import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchFormComponent } from './components/search-form/search-form.component';
import { GiphyCardComponent } from './components/giphy-card/giphy-card.component';
import { HomeComponent } from './pages/home/home.component';
import { ReactiveFormsModule } from '@angular/forms';

import { ShareModule } from 'src/app/share/share.module';

@NgModule({
  declarations: [SearchFormComponent, GiphyCardComponent, HomeComponent],
  imports: [CommonModule, ReactiveFormsModule, ShareModule]
})
export class HomeModule {}
